# import packages
import random
import argparse
import subprocess as sp
import os


# Class for all the information for each course
class CourseInfo(object):
    # Constructor
    def __init__(self, crn=0, course=None, professor=None, course_size=-1, need_multimedia=False):
        self.crn = crn
        self.course = course
        self.professor = professor
        self.course_size = course_size
        self.need_multimedia = need_multimedia

    # Override of the default == statement
    def __eq__(self, other):
        other = (CourseInfo)(other)
        return (self.crn == other.crn and self.course == other.course and
                self.course_size == other.course_size and self.need_multimedia == other.need_multimedia)


# Class for storing the information for each room
class Rooms(object):
    # Constructor
    def __init__(self, roomID=0, room=None, room_size=-1, multimedia=False):
        self.roomID = roomID
        self.room = room
        self.room_size = room_size
        self.multimedia = multimedia

    # Override of the default == method
    def __eq__(self, other):
        other = (Rooms)(other)
        return self.room == other.room and self.room_size == other.room_size and self.multimedia == other.multimedia


# Class for storing the information for each time slot
class Times(object):
    # Constructor
    def __init__(self, periodID=0, days=None, time=None):
        self.periodID = periodID
        self.days = days
        self.time = time

    # Override of the default == method
    def __eq__(self, other):
        other = (Times)(other)
        return self.periodID == other.periodID and self.days == other.days and self.time == other.time


# Class for storing all the information for a specific course
class Course(object):
    # Constructor
    def __init__(self, cID=CourseInfo(), rID=Rooms(), tID=Times()):
        self.cID = CourseInfo(cID.crn, cID.course, cID.professor, cID.course_size, cID.need_multimedia)
        self.rID = Rooms(rID.roomID, rID.room, rID.room_size, rID.multimedia)
        self.tID = Times(tID.periodID, tID.days, tID.time)

    # Override of the default == method
    def __eq__(self, other):
        other = (Course)(other)
        return self.cID == other.cID and self.rID == other.rID and self.tID == other.tID

    # Function to print out all the information of a course to a text file
    def print_course(self, out_file=None):
        if out_file is not None:
            out_file.write("{} ".format(self.cID.crn))
            out_file.write("{} ".format(self.cID.course))
            out_file.write("{} ".format(self.cID.professor))
            out_file.write("{} ".format(self.cID.course_size))
            out_file.write("{} | ".format(self.cID.need_multimedia))
            out_file.write("{} ".format(self.rID.room))
            out_file.write("{} ".format(self.rID.room_size))
            out_file.write("{} | ".format(self.rID.multimedia))
            out_file.write("{} ".format(self.tID.days))

            if "\n" in self.tID.time:
                out_file.write("{}".format(self.tID.time))
            else:
                out_file.write("{}".format(self.tID.time))
        else:
            print("{} ".format(self.cID.crn), end=" ")
            print("{} ".format(self.cID.course), end=" ")
            print("{} ".format(self.cID.professor), end=" ")
            print("{} ".format(self.cID.course_size), end=" ")
            print("{} | ".format(self.cID.need_multimedia), end=" ")
            print("{} ".format(self.rID.room), end=" ")
            print("{} ".format(self.rID.room_size), end=" ")
            print("{} | ".format(self.rID.multimedia), end=" ")
            print("{} ".format(self.tID.days), end=" ")

            if "\n" in self.tID.time:
                print("{}".format(self.tID.time), end="")
            else:
                print("{}".format(self.tID.time))


# Class to hold 27 courses or a list of courses
class Schedule(object):
    # Constructor
    def __init__(self, schedule=[Course()], fitness=0):
        self.schedule = schedule
        self.fitness = fitness

        for i in range(len(schedule)):
            self.schedule[i] = Course(schedule[i].cID, schedule[i].rID, schedule[i].tID)

    # Function to calculate the fitness of a certain schedule
    def calculate_fitness(self):
        self.fitness = 0
        for i in range(0, len(self.schedule)):
            same_room_time = 0

            if self.schedule[i].cID.need_multimedia:
                if self.schedule[i].rID.multimedia:
                    self.fitness += 20
                else:
                    self.fitness -= 50

            if int(self.schedule[i].cID.course_size) > int(self.schedule[i].rID.room_size):
                self.fitness -= 70
            elif int(self.schedule[i].cID.course_size) <= int(self.schedule[i].rID.room_size):
                self.fitness += 20

            for j in range(i, len(self.schedule)):
                if i == j:
                    continue

                if self.schedule[i].rID.room == self.schedule[j].rID.room:
                    if (self.schedule[i].tID.time == self.schedule[j].tID.time) and (self.schedule[i].tID.days == self.schedule[j].tID.days):
                        same_room_time += 1

                if self.schedule[i].cID.professor == self.schedule[j].cID.professor:
                    if self.schedule[i].tID.time == self.schedule[j].tID.time and self.schedule[i].tID.days == self.schedule[j].tID.days:
                        self.fitness -= 300

            if same_room_time == 1 or same_room_time == 2:
                self.fitness -= 300
            elif same_room_time == 3:
                self.fitness -= 600


generation = 0  # Global variable for the number of generations
best_schedule = Schedule()  # Global variable for the best schedule found


# Function for printing all the schedules, fitness scores and the courses in that schedule to the terminal
def print_schedules(population, best_fitness, min_fitness, average_fitness):
    global generation  # get the global generation value

    # Print out all schedules
    print("Generation: {}".format(generation))
    for j in range(len(population)):
        for item in population[j].schedule:
            item.print_course()
        print("Fitness: {0}\n".format(population[j].fitness))

    # Print min, best and average fitness
    print("Min Fitness: {0}\tAverage Fitness: {1}\tBest Fitness: {2}".format(min_fitness, average_fitness, best_fitness))

    # Print statement to format output
    print("-" * 100)


# Function for finding the best schedule
def check_best_schedule(population):
    global best_schedule  # Get the global value of best schedule

    for i in range(0, len(population)):
        if best_schedule.fitness < population[i].fitness:
            best_schedule = Schedule(population[i].schedule, population[i].fitness)


# Function for getting all the data from the files and storing them in arrays
def get_data():
    course_file = "courselist.csv"
    room_file = "roominfo.csv"
    week_file = "weekschedule.csv"

    open_course = open(course_file, 'r')
    open_room = open(room_file, 'r')
    open_week = open(week_file, 'r')

    course_info = open_course.readlines()
    room_info = open_room.readlines()
    week_info = open_week.readlines()

    open_course.close()
    open_room.close()
    open_week.close()

    temp_courses = []
    temp_rooms = []
    temp_times = []

    for item in course_info:
        entries = item.split(',')
        if entries[4] == 'X\n' or entries[4] == 'x\n':
            course = CourseInfo(entries[0], entries[1], entries[2], entries[3], True)
        else:
            course = CourseInfo(entries[0], entries[1], entries[2], entries[3])
        temp_courses.append(course)

    room_count = 0
    for item in room_info:
        entries = item.split(',')
        if entries[2] == 'X\n' or entries[2] == 'x\n':
            temp_room = Rooms(room_count, entries[0], entries[1], True)
            room_count += 1
        else:
            temp_room = Rooms(room_count, entries[0], entries[1])
            room_count += 1
        temp_rooms.append(temp_room)

    for item in week_info:
        entries = item.split(',')
        temp_week = Times(entries[0], entries[1], entries[2])
        temp_times.append(temp_week)

    return temp_courses, temp_rooms, temp_times


# Function for executing tournament style selection
def tournament_selection(population, N):
    winner = Schedule()  # Variable for the winning schedule
    loop = False  # Variable to loop around

    # Variables for randomly produced indexes
    random_index1, random_index2, random_index3 = 0, 0, 0

    # Loop till a proper schedule is found
    while not loop:
        # Generate random indexes
        random_index1 = random.randint(0, int(N) - 1)
        random_index2 = random.randint(0, int(N) - 1)
        random_index3 = random.randint(0, int(N) - 1)

        # Make sure none of the indexes are the same
        if random_index1 != random_index2 and random_index2 != random_index3 and random_index1 != random_index3:
            if population[random_index1].fitness == population[random_index2].fitness and population[random_index2].fitness < population[random_index3].fitness:
                continue
            else:
                loop = True

    # If the fitness of the schedule at index 1 is greater than that of index 2 and 3 that is the winner
    if population[random_index1].fitness > population[random_index2].fitness and population[random_index1].fitness > population[random_index3].fitness:
        winner = Schedule(population[random_index1].schedule, population[random_index1].fitness)
    elif population[random_index2].fitness > population[random_index1].fitness and population[random_index2].fitness > population[random_index3].fitness:
        winner = Schedule(population[random_index2].schedule, population[random_index2].fitness)
    else:
        winner = Schedule(population[random_index3].schedule, population[random_index3].fitness)

    # Return the winning schedule
    return winner


# Function for performing crossover between two parent schedules
def crossover(parent1, parent2, Pc):
    # Generate random number to see if crossover should be done
    do_crossover = random.randint(1, 99)

    # If random number is less than or equal to the probability of crossover than do crossover
    if do_crossover <= Pc:
        # Generate random number for start position of crossover
        crossover_point = random.randint(0, 26)

        # Do crossover from crossover point to the end of the schedule
        for i in range(crossover_point, 27):
            tempRoom = Rooms(parent1.schedule[i].rID.roomID, parent1.schedule[i].rID.room, parent1.schedule[i].rID.room_size, parent1.schedule[i].rID.multimedia)
            tempTime = Times(parent1.schedule[i].tID.periodID, parent1.schedule[i].tID.days, parent1.schedule[i].tID.time)

            parent1.schedule[i].rID = Rooms(parent2.schedule[i].rID.roomID, parent2.schedule[i].rID.room, parent2.schedule[i].rID.room_size, parent2.schedule[i].rID.multimedia)
            parent1.schedule[i].tID = Times(parent2.schedule[i].tID.periodID, parent2.schedule[i].tID.days, parent2.schedule[i].tID.time)

            parent2.schedule[i].rID = Rooms(tempRoom.roomID, tempRoom.room, tempRoom.room_size, tempRoom.multimedia)
            parent2.schedule[i].tID = Times(tempTime.periodID, tempTime.days, tempTime.time)


# Function for performing mutation on a specific parent
def mutation(parent, Pm, times, rooms):
    # Generate random to see if mutation will be executed
    do_mutation = random.randint(1, 99)

    # If random number generated less than or equal to the probability of mutation then do mutation
    if do_mutation <= Pm:
        # Generate a random number for the course that will be mutated
        mutation_point = random.randint(0, len(parent.schedule) - 1)

        # Generate random numbers to decide what the new room and time will be
        next_roomID = random.randint(0, 8)
        nexttID = random.randint(0, 13)

        # Change the room to the new randomly picked room
        parent.schedule[mutation_point].rID = Rooms(rooms[next_roomID].roomID, rooms[next_roomID].room, rooms[next_roomID].room_size, rooms[next_roomID].multimedia)

        # Change the time to the new randomly picked time
        parent.schedule[mutation_point].tID = Times(times[nexttID].periodID, times[nexttID].days, times[nexttID].time)


# Function to find the minimum fitness
def find_min_fitness(population):
    min_fitness = 25000

    # Loop to find the smallest fitness score
    for i in range(0, len(population)):
        schedule = population[i]

        if schedule.fitness < min_fitness:
            min_fitness = schedule.fitness

    return min_fitness


# Function to find the average fitness
def find_average_fitness(population):
    average_fitness = 0

    # Loop to find the average fitness
    for i in range(0, len(population)):
        schedule = population[i]

        average_fitness += schedule.fitness

    average_fitness /= 50
    average_fitness = round(average_fitness)
    return average_fitness


# Function to find the best fitness in a population
def find_best_fitness(population):
    best_fitness = -25000

    # Loop to see if the population contains a fitness better than the current best
    for i in range(0, len(population)):
        schedule = population[i]

        if schedule.fitness > best_fitness:
            best_fitness = schedule.fitness

    return best_fitness


# Function for creating a new population
def create_new_population(population, Pc, Pm, times, rooms, N):
    new_list = []  # Initialize empty list

    # Loop for N, increment by 2 because we will be adding two parents to the list every time
    for i in range(0, int(N), 2):
        # Create parent schedules
        parentA, parentB = Schedule(), Schedule()

        # Do selection till parentA and parentB have different fitness scores
        while parentA.fitness == parentB.fitness:
            parentA = tournament_selection(population, N)
            parentB = tournament_selection(population, N)

        # Call crossover function to see if crossover will be performed
        crossover(parentA, parentB, Pc)

        # Call mutation function with parentA
        mutation(parentA, Pm, times, rooms)

        # Call mutation function with parentB
        mutation(parentB, Pm, times, rooms)

        # Add both parents to the new population/list
        new_list.append(parentA)
        new_list.append(parentB)

    # Return the new_list once full
    return new_list


# Function for creating the intial population
def create_intial_population(N, courses, rooms, times):
    # Create list of schedule
    list_of_schedules = []

    # Loop to N to fill the list
    for i in range(0, int(N)):
        courseID = 0  # Variable for the course id

        # Create list to hold all courses per schedule
        Courses = []

        # Go through every course in courses and create a course add it the course list
        for courseinfo in courses:
            randomrID = random.randint(0, 8)
            randomtID = random.randint(0, 13)

            course = Course(courseinfo, rooms[randomrID], times[randomtID])
            Courses.append(course)
            courseID += 1

        # Create schedule using courses list and give beginning fitness of 0
        schedule = Schedule(Courses, 0)

        # Add schedule to list of schedules
        list_of_schedules.append(schedule)

    # Return list_of_schedules which is the initial population
    return list_of_schedules


def get_input():
    N = input("Enter in N value(population size): ")
    max_generations = int(input("Enter in the max number of generations: "))
    Pc = float(input("Enter in the crossover probability: ")) * 100
    Pc = int(Pc)
    Pm = float(input("Enter in the mutation probability: ")) * 100
    Pm = int(Pm)

    return N, max_generations, Pc, Pm


# Main method
def __main__(N="", max_generations=0, Pc=0, Pm=0):
    # Get the global values for generation and best fitness
    global generation, best_schedule

    # Get input for N, max generations, probability of crossover(Pc), probability of mutation(Pm)
    # Get Pc and Pm as floats multiple them by 100 and then convert them into a float
    if N is None and max_generations == 0 and Pc == 0 and Pm == 0:
        N, max_generations, Pc, Pm = get_input()

    # Print statment to format output
    print()

    # Get all the courses, rooms and times
    courses, rooms, times = get_data()

    # Create initial population
    population = create_intial_population(N, courses, rooms, times)

    # Loop till max generation is reach
    for i in range(0, max_generations + 1):
        generation = i  # Make generation equal to i to keep track of what generation we are at

        # Calculate fitness for every schedule in the population
        for j in range(0, len(population)):
            population[j].calculate_fitness()

        # For loop to check if a population contains the best schedule possible
        # If it does print out the population and exit program
        for j in range(0, len(population)):
            if population[j].fitness >= 820:
                print("Best schedule found in generation: {}".format(generation))
                print(population[j].fitness)
                return

        # Print out every 100 generations
        if generation % 100 == 0:
            sp.call('printf "."', shell=True)  # print dots to know it is still running not going going to print entire schedules
            # print_schedules(population, best_fitness, min_fitness, average_fitness)
            check_best_schedule(population)

        # Create new_population
        new_population = create_new_population(population, Pc, Pm, times, rooms, N)

        # Nested for loop to change the old populations rooms and times to those of the new population
        for k in range(0, int(N)):
            for j in range(0, 27):
                population[k].schedule[j].rID = new_population[k].schedule[j].rID
                population[k].schedule[j].tID = new_population[k].schedule[j].tID

    # Print the best schedule
    sp.call('printf "\n"', shell=True)
    if os.path.exists("output.txt"):
        os.remove("output.txt")
    output_file = open('output.txt', 'a')
    output_file.write("Best Schedule:\n")
    for i in range(0, len(best_schedule.schedule)):
        best_schedule.schedule[i].print_course(out_file=output_file)
    output_file.write("Fitness: {0}\n".format(best_schedule.fitness))
    output_file.close()


# Call to main function
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--n', default="")
    parser.add_argument('-m', '--max-generations', default=0)
    parser.add_argument('-pc', '--pc', default=0)
    parser.add_argument('-pm', '--pm', default=0)
    args = parser.parse_args()

    __main__(N=args.n, max_generations=int(args.max_generations),
             Pc=int(float(args.pc) * 100), Pm=int(float(args.pm) * 100))
